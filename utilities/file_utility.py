import fauxfactory
import random

class FileUtility:

    def alpha_generator(self, size=8, utf8=False):
        """
        Generates a random character string (Letters + Digits)
        Default length of string generated is 8
        Only considers ascii letters and digits
        """
        if utf8:
            alphasize = random.randint(1, size-1)
            utfsize = size - alphasize
            result = fauxfactory.gen_alpha(alphasize) + fauxfactory.gen_utf8(utfsize)
        else:
            result = fauxfactory.gen_alpha(size)
        return result

    def num_size_generator(self, size=8, utf8=False):
        """
        Generates a random character string (Letters + Digits)
        Default length of string generated is 8
        Only considers ascii letters and digits
        """
        if utf8:
            alphasize = random.randint(1, size-1)
            utfsize = size - alphasize
            result = fauxfactory.gen_alphanumeric(alphasize) + fauxfactory.gen_utf8(utfsize)
        else:
            result = fauxfactory.gen_numeric_string(size)
        return result
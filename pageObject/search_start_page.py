from pageobject.search_result_page import ResultPage
from webloators.test_uri import Uri
from selenium.webdriver.common.keys import Keys
import time


class StartPage:

    input = "//input[@class='gsfi'][@id='lst-ib']"

    def checkValidSearchResult(self, driver, str1, str2, str3):
        driver.get(Uri.prodUri)

        time.sleep(2)
        input_st1_form = driver.find_element_by_xpath(ResultPage.input_form)
        input_st1_form.send_keys(str1, Keys.RETURN)
        result_num = driver.find_element_by_xpath(ResultPage.search_result)
        str1_result = result_num.text

        time.sleep(2)
        input_str2_form = driver.find_element_by_xpath(ResultPage.input_form)
        input_str2_form.clear()
        input_str2_form.send_keys(str2, Keys.RETURN)
        str2_reuslt = result_num.text

        if str3 == str1 and str1_result >= str2_reuslt:
            return "pass"
        if str2 == str1 and str2_reuslt >= str1_result:
            return "pass"
        else:
            return "fail"






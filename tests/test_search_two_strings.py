from selenium import webdriver
from pageobject.search_start_page import StartPage
from utilities.file_utility import FileUtility

import unittest

class TestStringSearch(unittest.TestCase):
    gu = FileUtility()

    def setUp(self):
        self.dr = webdriver.Chrome()

    def test_001_check_input_two_string_result(self):

        new_search = StartPage()
        input_str1 = self.gu.alpha_generator(size=10, utf8=False)
        input_str2 = self.gu.alpha_generator(size=4, utf8=False)

        if new_search.checkValidSearchResult(self.dr, str1=input_str1, str2=input_str2, str3=input_str1) == "pass":
            self.assertTrue(True)
        else:
            self.assertFalse(True)

if __name__ == '__main__':
    unittest.main()